﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.Linq;
using Spire.Doc;
using Accord.Math;
using Microsoft.ML.Transforms.Text;
using CsvHelper;
using Extreme.DataAnalysis;
using Extreme.Mathematics;

namespace CVBagOfWords
{
    public class SimilarityRanking
    {
        private static MLContext mlContext;
        private static string dir_path = "C:/Users/ben/Documents/attachments2/Junior Software Engineer/CVs accepted";
        private static string dir_path2 = "C:/Users/ben/Documents/attachments";
        private static string train_path = "C:/Users/ben/Documents/attachments/train";
        private static string desc_path = "C:/Users/ben/Documents/attachments/" +
                "job descriptions/Developer.txt";
        private static string desc_path2 = "C:/Users/ben/Documents/attachments/" +
                "job descriptions/System Developer.txt";
        private static string desc_path3= "C:/Users/ben/Documents/attachments/" +
                "job descriptions/Junior Java Developer.txt";
        private static string csv_path = "C:/Users/ben/Documents/CVRanking.csv";
        private static string doc_text;
        private static string pdf_text;
        public static string job_text;
        public static string job_text2;
        public static string job_text3;
        
        private static List<double> sim_scores = new List<double>();
        public static List<double> sim_scores2 = new List<double>();

        private static List<double> precision = new List<double>();
        private static List<double> precision2 = new List<double>();

        public static void SimilarityDocumentsRanking(string jobad)
        {
            Console.WriteLine("Retrieving the CVs similar to Junior Software Engineer");
            //retrieve the files in the directory with cv files
            string[] fileentries = Directory.GetFiles(dir_path);
            //get the job avdertisemnt
            job_text = GetTxtText(jobad);

            foreach (string filename in fileentries)
            {

                //Get the file extension to determine if it is a doc file or pdf file
                FileInfo file_type = new FileInfo(filename);
                if (file_type.Extension == ".docx" || file_type.Extension == ".doc")
                {
                    Console.WriteLine("Adding: {0}...", Path.GetFileName(filename));
                    doc_text = GetTextDoc(filename);
                    //create a vector for each cv file and the job requirements
                    WordEmbedding50D(doc_text, job_text);

                }
                else if (file_type.Extension == ".pdf")
                {
                    Console.WriteLine("Adding: {0}...", Path.GetFileName(dir_path));
                    pdf_text = GetTextPdf(filename);

                }
                else
                {
                    Console.WriteLine("{0} is not a valid file or directory.", dir_path);
                }

            }

            List<string> file_extension = new List<string>();
            foreach (string file in fileentries)
            {

                file_extension.Add(Path.GetFileName(file));
            }
            var map = CalculateMAP();
           
            Console.WriteLine("Similarity score ranking");
            var dic = file_extension.Zip(sim_scores2, (k, v) => new { k, v })
              .ToDictionary(x => x.k, x => x.v);
            dic = dic.OrderByDescending(u => u.Value).ToDictionary(z => z.Key, y => y.Value);

            dic.Select(i => $"{i.Key}: {i.Value}").ToList().ForEach(Console.WriteLine);
            Console.WriteLine();
            
            Console.WriteLine("MAP score ranking");
            var dic2 = file_extension.Zip(precision2, (k, v) => new { k, v })
              .ToDictionary(x => x.k, x => x.v);
            dic2 = dic2.OrderByDescending(u => u.Value).ToDictionary(z => z.Key, y => y.Value);
           
            dic2.Select(i => $"{i.Key}: {i.Value}").ToList().ForEach(Console.WriteLine);
            Console.WriteLine();

            //Console.WriteLine("Mean Average Precision: ", map);

            /**var result = dic.Concat(dic2).GroupBy(d => d.Key)
             .ToDictionary(d => d.Key, d => d.First().Value);
            result.Select(i => $"{i.Key}: {i.Value}").ToList().ForEach(Console.WriteLine);
            Console.WriteLine();**/

            /**Write the results of the scores to a csv file**/
            using (var writer = new System.IO.StreamWriter(@"C:/Users/ben/Documents/job_cv.csv"))
            {
                using (var csv = new CsvHelper.CsvWriter(writer, System.Globalization.CultureInfo.InvariantCulture))
                {
                    foreach (var file in file_extension)
                    {
                        csv.WriteField(file);
                        csv.NextRecord();
                    }
                    

                    writer.Flush();
                }
            }
            using (var stream = File.Open(@"C:/Users/ben/Documents/job_cv.csv", FileMode.Append))
            using (var writer = new StreamWriter(stream))   
            {
                using (var csv = new CsvHelper.CsvWriter(writer, System.Globalization.CultureInfo.InvariantCulture))
                {

                    foreach (var sim in sim_scores2)
                    {
                        csv.WriteField(sim);
                        csv.NextRecord();
                    }
                    foreach(var num in precision2)
                    {
                        csv.WriteField(num);                       
                        csv.NextRecord();
                    }

                    writer.Flush();
                }
            }
        }
        public static void SimilarityDocumentsRankingJob1()
        {
            string[] fileentries = Directory.GetFiles(dir_path2);
            //string[] jobentries = Directory.GetFiles(desc_path2);
            string[] trainfiles = Directory.GetFiles(train_path);
            job_text = GetTxtText(desc_path);
            
            foreach (string filename in fileentries)
            {
                
                    //Get the file extension to determine if it is a doc file or pdf file
                    FileInfo file_type = new FileInfo(filename);
                    if (file_type.Extension == ".docx" || file_type.Extension == ".doc")
                    {
                        Console.WriteLine("Adding: {0}...", Path.GetFileName(filename));
                        doc_text = GetTextDoc(filename);
                        
                        WordEmbedding50D(doc_text, job_text);
                        
                    }
                    else if (file_type.Extension == ".pdf")
                    {
                        Console.WriteLine("Adding: {0}...", Path.GetFileName(dir_path2));
                        pdf_text = GetTextPdf(filename);

                    }
                    else
                    {
                        Console.WriteLine("{0} is not a valid file or directory.", dir_path2);
                    }
                

            }
            List<string> file_extension = new List<string>();
            foreach (string file in fileentries)
            {

                file_extension.Add(Path.GetFileName(file));
            }
            var map = CalculateMAP();

            Console.WriteLine("Similarity score ranking");
            var dic = file_extension.Zip(sim_scores2, (k, v) => new { k, v })
              .ToDictionary(x => x.k, x => x.v);
            dic = dic.OrderByDescending(u => u.Value).ToDictionary(z => z.Key, y => y.Value);

            dic.Select(i => $"{i.Key}: {i.Value}").ToList().ForEach(Console.WriteLine);
            Console.WriteLine();
            
            
            Console.WriteLine("MAP score ranking");
            var dic2 = file_extension.Zip(precision2, (k, v) => new { k, v })
              .ToDictionary(x => x.k, x => x.v);
            dic2 = dic2.OrderByDescending(u => u.Value).ToDictionary(z => z.Key, y => y.Value);

            dic2.Select(i => $"{i.Key}: {i.Value}").ToList().ForEach(Console.WriteLine);
            Console.WriteLine();
            //Console.WriteLine("Mean Average Precision: ", map);


            using (var writer = new System.IO.StreamWriter(@"C:/Users/ben/Documents/job1_cv.csv"))
            {
                using (var csv = new CsvHelper.CsvWriter(writer, System.Globalization.CultureInfo.InvariantCulture))
                {
                    foreach (var file in file_extension)
                    {
                        csv.WriteField(file);
                        csv.NextRecord();
                    }
                    

                    writer.Flush();
                }
            }
            using (var stream = File.Open(@"C:/Users/ben/Documents/job1.csv", FileMode.Append))
            using (var writer = new StreamWriter(stream))   
            {
                using (var csv = new CsvHelper.CsvWriter(writer, System.Globalization.CultureInfo.InvariantCulture))
                {

                    foreach (var sim in sim_scores2)
                    {
                        csv.WriteField(sim);
                        csv.NextRecord();
                    }
                    foreach(var num in precision2)
                    {
                        csv.WriteField(num);                       
                        csv.NextRecord();
                    }

                    writer.Flush();
                }
            }

        }

        

        public static void SimilarityDocumentsRankingJob2()
        {
            string[] fileentries = Directory.GetFiles(dir_path2);
            
            job_text2 = GetTxtText(desc_path2);
                       

            foreach (string filename in fileentries)
            {

                //Get the file extension to determine if it is a doc file or pdf file
                FileInfo file_type = new FileInfo(filename);
                if (file_type.Extension == ".docx" || file_type.Extension == ".doc")
                {
                    Console.WriteLine("Adding: {0}...", Path.GetFileName(filename));
                    doc_text = GetTextDoc(filename);

                    WordEmbedding50D(doc_text, job_text2);
                  
                }
                else if (file_type.Extension == ".pdf")
                {
                    Console.WriteLine("Adding: {0}...", Path.GetFileName(dir_path2));
                    pdf_text = GetTextPdf(filename);

                }
                else
                {
                    Console.WriteLine("{0} is not a valid file or directory.", dir_path2);
                }


            }
            List<string> file_extension = new List<string>();
            foreach (string file in fileentries)
            {

                file_extension.Add(Path.GetFileName(file));
            }
            var map = CalculateMAP();

            Console.WriteLine("Similarity score ranking");
            var dic = file_extension.Zip(sim_scores2, (k, v) => new { k, v })
              .ToDictionary(x => x.k, x => x.v);
            dic = dic.OrderByDescending(u => u.Value).ToDictionary(z => z.Key, y => y.Value);

            dic.Select(i => $"{i.Key}: {i.Value}").ToList().ForEach(Console.WriteLine);
            Console.WriteLine();
            
            
            Console.WriteLine("MAP score ranking");
            var dic2 = file_extension.Zip(precision2, (k, v) => new { k, v })
              .ToDictionary(x => x.k, x => x.v);
            dic2 = dic2.OrderByDescending(u => u.Value).ToDictionary(z => z.Key, y => y.Value);

            dic2.Select(i => $"{i.Key}: {i.Value}").ToList().ForEach(Console.WriteLine);
            Console.WriteLine();
            //Console.WriteLine("Mean Average Precision: ", map);


            using (var writer = new System.IO.StreamWriter(@"C:/Users/ben/Documents/job2_cv.csv"))
            {
                using (var csv = new CsvHelper.CsvWriter(writer, System.Globalization.CultureInfo.InvariantCulture))
                {
                    foreach (var file in file_extension)
                    {
                        csv.WriteField(file);
                        csv.NextRecord();
                    }


                    writer.Flush();
                }
            }
            using (var stream = File.Open(@"C:/Users/ben/Documents/job3cv.csv", FileMode.Append))
            using (var writer = new StreamWriter(stream))
            {
                using (var csv = new CsvHelper.CsvWriter(writer, System.Globalization.CultureInfo.InvariantCulture))
                {

                    foreach (var sim in sim_scores2)
                    {
                        csv.WriteField(sim);
                        csv.NextRecord();
                    }
                    foreach (var num in precision2)
                    {
                        csv.WriteField(num);
                        csv.NextRecord();
                    }

                    writer.Flush();
                }
            }


        }

        public static void SimilarityDocumentsRankingJob3()
        {
            string[] fileentries = Directory.GetFiles(dir_path2);

            job_text3 = GetTxtText(desc_path3);


            foreach (string filename in fileentries)
            {

                //Get the file extension to determine if it is a doc file or pdf file
                FileInfo file_type = new FileInfo(filename);
                if (file_type.Extension == ".docx" || file_type.Extension == ".doc")
                {
                    Console.WriteLine("Adding: {0}...", Path.GetFileName(filename));
                    doc_text = GetTextDoc(filename);

                    WordEmbedding50D(doc_text, job_text3);

                }
                else if (file_type.Extension == ".pdf")
                {
                    Console.WriteLine("Adding: {0}...", Path.GetFileName(dir_path2));
                    pdf_text = GetTextPdf(filename);

                }
                else
                {
                    Console.WriteLine("{0} is not a valid file or directory.", dir_path2);
                }


            }

            List<string> file_extension = new List<string>();
            foreach (string file in fileentries)
            {

                file_extension.Add(Path.GetFileName(file));
            }
            var map = CalculateMAP();

            Console.WriteLine("Similarity score ranking");
            var dic = file_extension.Zip(sim_scores2, (k, v) => new { k, v })
              .ToDictionary(x => x.k, x => x.v);
            dic = dic.OrderByDescending(u => u.Value).ToDictionary(z => z.Key, y => y.Value);

            dic.Select(i => $"{i.Key}: {i.Value}").ToList().ForEach(Console.WriteLine);
            Console.WriteLine();
            
            
            Console.WriteLine("MAP score ranking");
            var dic2 = file_extension.Zip(precision2, (k, v) => new { k, v })
              .ToDictionary(x => x.k, x => x.v);
            dic2 = dic2.OrderByDescending(u => u.Value).ToDictionary(z => z.Key, y => y.Value);

            dic2.Select(i => $"{i.Key}: {i.Value}").ToList().ForEach(Console.WriteLine);
            Console.WriteLine();
            //Console.WriteLine("Mean Average Precision: ", map);


            using (var writer = new System.IO.StreamWriter(@"C:/Users/ben/Documents/job3_cv.csv"))
            {
                using (var csv = new CsvHelper.CsvWriter(writer, System.Globalization.CultureInfo.InvariantCulture))
                {
                    foreach (var file in file_extension)
                    {
                        csv.WriteField(file);
                        csv.NextRecord();
                    }


                    writer.Flush();
                }
            }
            using (var stream = File.Open(@"C:/Users/ben/Documents/job3_cv.csv", FileMode.Append))
            using (var writer = new StreamWriter(stream))
            {
                using (var csv = new CsvHelper.CsvWriter(writer, System.Globalization.CultureInfo.InvariantCulture))
                {

                    foreach (var sim in sim_scores2)
                    {
                        csv.WriteField(sim);
                        csv.NextRecord();
                    }
                    foreach (var num in precision2)
                    {
                        csv.WriteField(num);
                        csv.NextRecord();
                    }

                    writer.Flush();
                }
            }


        }

        public static double CalculateMAP()
        {

            var rankings = sim_scores.OrderByDescending(x => x);

            var rank = sim_scores2.OrderByDescending(x => x);
            double sum = 0;
            double count = 0;

            foreach(double value in rankings)
            {
                sum += value;
                count++;
                precision.Add(sum / count);
            }

            foreach (double value in rank)
            {
                sum += value;
                count++;
                precision2.Add(sum / count);
            }


            /**foreach (double p in precision2)
            {
                Console.WriteLine("----------Precision scores---------------");
                Console.WriteLine(p);
            }**/
            

            return precision2.Average();

            
        }

        
        //Cosine similarity function
        public static double Similarity(double[] x, double[] y)
        {
            double sum = 0;
            double p = 0;
            double q = 0;

            for (int i = 0; i < x.Length; i++)
            {
                sum += x[i] * y[i];
                p += x[i] * x[i];
                q += y[i] * y[i];
            }

            double den = Math.Sqrt(p) * Math.Sqrt(q);
            return (sum == 0) ? 0 : sum / den;
        }

        //Function to extract text documents
        public static string GetTextDoc(string path)
        {
            Document document = new Document();
            document.LoadFromFile(path);

            //Initialzie StringBuilder Instance
            StringBuilder sb = new StringBuilder();

            //Extract Text from Word and Save to StringBuilder Instance
            /**foreach (Spire.Doc.Section section in document1.Sections)
            {
                foreach (Spire.Doc.Documents.Paragraph paragraph in section.Paragraphs)

                {
                    sb.AppendLine(paragraph.Text);
                }
            }
            return sb.ToString();**/
            string text = document.GetText();
            return text;

        }

        //Function to extract PDF documents
        public static string GetTextPdf(string path)
        {
            Spire.Pdf.PdfDocument pdoc = new Spire.Pdf.PdfDocument();
            pdoc.LoadFromFile(path);

            //Initialzie StringBuilder Instance for pdf
            StringBuilder sbpdf = new StringBuilder();
            //Extract text from all pages
            foreach (Spire.Pdf.PdfPageBase page in pdoc.Pages)
            {
                sbpdf.Append(page.ExtractText());
            }

            return sbpdf.ToString();
        }

        //Function to extract job description
        public static string GetTxtText(string path)
        {
            string text = System.IO.File.ReadAllText(path);

            return text.ToString();
        }

        //Word embeddings text pipeline
        public static IEstimator<ITransformer> TextPipeline()
        {
            //create the transformation pipeline
            var textPipeline = mlContext.Transforms.Text.NormalizeText("Text")
                .Append(mlContext.Transforms.Text.TokenizeIntoWords("Tokens", "Text"))
                .Append(mlContext.Transforms.Text.RemoveDefaultStopWords("Tokens", "Tokens",
                    Microsoft.ML.Transforms.Text.StopWordsRemovingEstimator.Language.English))
                .Append(mlContext.Transforms.Text.ApplyWordEmbedding("Features", "Tokens",
                WordEmbeddingEstimator.PretrainedModelKind.GloVe50D));
            return textPipeline;
        }

        public static IEstimator<ITransformer> TextPipeline2()
        {
            //create the transformation pipeline
            var textPipeline = mlContext.Transforms.Text.NormalizeText("Text")
                .Append(mlContext.Transforms.Text.TokenizeIntoWords("Tokens", "Text"))
                .Append(mlContext.Transforms.Text.RemoveDefaultStopWords("Tokens", "Tokens",
                    Microsoft.ML.Transforms.Text.StopWordsRemovingEstimator.Language.English))
                .Append(mlContext.Transforms.Text.ApplyWordEmbedding("Features", "Tokens",
                WordEmbeddingEstimator.PretrainedModelKind.GloVe100D));
            return textPipeline;
        }


        public static void WordEmbedding50D(string filename, string jobdesc)
        {
            //Initialize an ML context
            mlContext = new MLContext();

            //Load the extracted text from doc file to Input class
            var doc_data = new Input() { Text = filename };


            //Create an empty list class to hold all the inputs from the file
            var emptylist = new List<Input>();
            //convert list into dataview
            var emptyDataView = mlContext.Data.LoadFromEnumerable(emptylist);

            //Initialize the word embeddings pipeline 
            /**Word embeddings model uses Glove which is a pretrained model with 50Dimensions**/
            var wordemb_pipeline = TextPipeline();


            //Fit the pipeline with the empty data view 
            var textTransformer = wordemb_pipeline.Fit(emptyDataView);
            //prediction engine
            var predictionEngine = mlContext.Model.CreatePredictionEngine<Input, TransformedText>(textTransformer);

            //Job description data
            var job_data = new Input() { Text = jobdesc };

            //call the prediction API
            var prediction = predictionEngine.Predict(doc_data);

            //predict job description data
            var job_pred = predictionEngine.Predict(job_data);

            double[] cvvector = new double[(prediction.Features.Length)/3];
            Array.Copy(prediction.Features, (prediction.Features.Length) / 3, cvvector,0, (prediction.Features.Length) /3);
            Console.WriteLine();
            

            double[] jobvector = new double[(job_pred.Features.Length)/3];
            Array.Copy(prediction.Features, (prediction.Features.Length) / 3, jobvector, 0, (prediction.Features.Length) / 3);
            Console.WriteLine();
           

            //Calculate the similarity using using the Similarity function
            var similarity_vec = Similarity(cvvector, jobvector);
            sim_scores.Add(similarity_vec);
            //Console.WriteLine("Cosine similarity score between the job decsription and cv document: ");
            //Console.WriteLine(similarity_vec);
            Console.WriteLine();

            double[] vector1 = new double[(prediction.Features.Length)];
            Array.Copy(prediction.Features, vector1, prediction.Features.Length);
            Console.WriteLine();
            

            double[] vector2 = new double[(job_pred.Features.Length)];
            Array.Copy(job_pred.Features, vector2, job_pred.Features.Length);
            Console.WriteLine();
            

            //Calculate the similarity using using the Similarity function
            similarity_vec = Similarity(vector1, vector2);
            sim_scores2.Add(similarity_vec);



        }

        public static void WordEmbedding100D(string filename, string jobdesc)
        {
            //Initialize an ML context
            mlContext = new MLContext();

            //Load the extracted text from doc file to Input class
            var doc_data = new Input() { Text = filename };


            //Create an empty list class to hold all the inputs from the file
            var emptylist = new List<Input>();
            //convert list into dataview
            var emptyDataView = mlContext.Data.LoadFromEnumerable(emptylist);

            //Initialize the word embeddings pipeline 
            /**Word embeddings model uses Glove which is a pretrained model with 50Dimensions**/
            var wordemb_pipeline = TextPipeline2();


            //Fit the pipeline with the empty data view 
            var textTransformer = wordemb_pipeline.Fit(emptyDataView);
            //prediction engine
            var predictionEngine = mlContext.Model.CreatePredictionEngine<Input, TransformedText>(textTransformer);

            //Job description data
            var job_data = new Input() { Text = jobdesc };

            //call the prediction API
            var prediction = predictionEngine.Predict(doc_data);

            //predict job description data
            var job_pred = predictionEngine.Predict(job_data);

            double[] vector1 = new double[(prediction.Features.Length)];
            Array.Copy(prediction.Features, vector1, prediction.Features.Length);
            Console.WriteLine();
            //Console.WriteLine(vector1[7]);

            double[] vector2 = new double[(job_pred.Features.Length)];
            Array.Copy(job_pred.Features, vector2, job_pred.Features.Length);
            Console.WriteLine();
            //Console.WriteLine(vector2[9]);

            //Calculate the similarity using using the Similarity function
            var similarity_vec = Similarity(vector1, vector2);
            sim_scores2.Add(similarity_vec);
            //Console.WriteLine("Cosine similarity score with two vectors");
            //Console.WriteLine(similarity_vec);
            //Console.WriteLine();

        }

    }
}
