﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Trainers;
using Microsoft.ML.Transforms.Text;
using Accord.Math;
using System.Linq;

namespace CVBagOfWords
{
    public class KMeans
    {
        private static MLContext mlContext;
        private static string csv_path = "C:/Users/ben/Documents/attachments/BOG_CV.csv";
        //static readonly string _modelPath = Path.Combine(Environment.CurrentDirectory, "DataModel", "CVsClusteringModel.zip");

        public static void KmeansCluster()
        {
            mlContext = new MLContext(seed: 0);

            IDataView data = mlContext.Data.LoadFromTextFile<BOGData>(csv_path, hasHeader: true);

            //split data into train and test set
            DataOperationsCatalog.TrainTestData dataSplit = mlContext.Data.TrainTestSplit(data, testFraction: 0.2);
            IDataView trainData = dataSplit.TrainSet;
            IDataView testData = dataSplit.TestSet;

            var bog_data = mlContext.Transforms.DropColumns("CVS");
            var bog_trainTransform = bog_data.Fit(trainData).Transform(trainData);
            var bog_testTransform = bog_data.Fit(testData).Transform(testData);

            //DisplayColumns(bog_trainTransform);

            //kmeans pipeline
            string featuresColmName = "Features";
            var kmeanspipeline = mlContext.Transforms.Concatenate(featuresColmName, "SystemDeveloper", "Developer", "DataAnalyst", "FrontEndDeveloper", "JuniorJavaDeveloper", "JuniorSystemDeveloper", "TechnicalProjectManager", "UXUIDesigner", "WebDeveloper")
            .Append(mlContext.Transforms.NormalizeLpNorm(featuresColmName))
            .Append(mlContext.Clustering.Trainers.KMeans(featuresColmName, numberOfClusters: 3));

            //fit the model on the train data
            var kmeans_model = kmeanspipeline.Fit(bog_trainTransform);

            //create a prediction engine to test the data
            var predictor = mlContext.Model.CreatePredictionEngine<BOGData, KmeansPrediction>(kmeans_model);

            //predict the cluster of a single text
            var cvpredictions = predictor.Predict(PredictionModel.cvbog);
            Console.WriteLine($"Cluster: {cvpredictions.PredictedClusterID}");
            Console.WriteLine($"Distances: {string.Join(" ", cvpredictions.Distances)}");

            var predictionstest = kmeans_model.Transform(bog_testTransform);

            var metrics = mlContext.Clustering.Evaluate(predictionstest);

            Console.WriteLine($"Average minimum score: {metrics.AverageDistance}");

            foreach(var p  in predictionstest.Schema)
            {
                Console.WriteLine($"Prediction: {p.Index}");
            }






        }

        private static void DisplayColumns(IDataView data)
        {
            var preview = data.Preview(maxRows: 7);

            string previewData = "";

            for (int i = 0; i < preview.RowView.Length; i++)
            {
                foreach (var item in preview.RowView[i].Values)
                {
                    previewData += $"{item.Key}: {item.Value} ";
                }

                Console.WriteLine("----------------------------------");
                Console.WriteLine(previewData);
                previewData = "";
            }
        }
    }

    public class BOGData
    {
        [LoadColumn(0)]
        public string CVS { get; set; }

        [LoadColumn(1)]
        public float SystemDeveloper { get; set; }

        [LoadColumn(2)]
        public float Developer { get; set; }

        [LoadColumn(3)]
        public float DataAnalyst { get; set; }

        [LoadColumn(4)]
        public float FrontEndDeveloper { get; set; }

        [LoadColumn(5)]
        public float JuniorJavaDeveloper { get; set; }

        [LoadColumn(6)]
        public float JuniorSystemDeveloper { get; set; }

        [LoadColumn(7)]
        public float TechnicalProjectManager { get; set; }

        [LoadColumn(8)]
        public float UXUIDesigner { get; set; }

        [LoadColumn(9)]
        public float WebDeveloper { get; set; }

    }

    public class KmeansPrediction
    {
        [ColumnName("PredictedLabel")]
        public uint PredictedClusterID;

        [ColumnName("Score")]
        public float [] Distances;

        public uint PredLabel { get; set; }
    }
}
