﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.IO;
using CsvHelper;

namespace CVBagOfWords
{
    public class KNN
    {
		public static List<string> trainingSetValues = new List<string>();
		private static List<double[]> trainingValues = new List<double[]>();
		private static List<string> trainingClass = new List<string>();

		public static List<string> testSetValues = new List<string>();
		private static List<double[]> testValues = new List<double[]>();
		private static List<string> testClass = new List<string>();

		private static int K;
		public static void KnnClassifier()
        {
			using (var reader = new System.IO.StreamReader(@"C:/Users/ben/Documents/test_cv.csv"))
			{
				using (var csv = new CsvHelper.CsvReader(reader, System.Globalization.CultureInfo.InvariantCulture))
				{					

					while (csv.Read())
					{
						var sim_scores = csv.GetField(1);

						trainingSetValues.Add(sim_scores);
					}
				}
			}

			double[] simscore = new double[trainingSetValues.Count];
			for(int i=0; i < trainingSetValues.Count; i++)
            {
				double score = Double.Parse(trainingSetValues.ElementAt(i));
				simscore[i] = score;
            }
			trainingValues.Add(simscore);

			using (var reader = new System.IO.StreamReader(@"C:/Users/ben/Documents/train_test_cv.csv"))
			{
				using (var csv = new CsvHelper.CsvReader(reader, System.Globalization.CultureInfo.InvariantCulture))
				{

					while (csv.Read())
					{
						var sim_scores = csv.GetField(1);

						testSetValues.Add(sim_scores);
					}
				}
			}

			double[] testsimscore = new double[testSetValues.Count];
			for (int i = 0; i < testSetValues.Count; i++)
			{
				double score = Double.Parse(testSetValues.ElementAt(i));
				testsimscore[i] = score;
			}
			testValues.Add(testsimscore);

			using (var reader = new System.IO.StreamReader(@"C:/Users/ben/Documents/train_test_cv.csv"))
			{
				using (var csv = new CsvHelper.CsvReader(reader, System.Globalization.CultureInfo.InvariantCulture))
				{

					while (csv.Read())
					{
						var class_name = csv.GetField(3);

						testClass.Add(class_name);
					}
				}
			}

			using (var reader = new System.IO.StreamReader(@"C:/Users/ben/Documents/train_test_cv.csv"))
			{
				using (var csv = new CsvHelper.CsvReader(reader, System.Globalization.CultureInfo.InvariantCulture))
				{

					while (csv.Read())
					{
						var class_name = csv.GetField(3);

						trainingClass.Add(class_name);
					}
				}
			}
		}

		public static void Classify(int neighborsNumber)
		{
			K = neighborsNumber;

			// create an array where we store the distance from our test data and the training data -> [0]
			// plus the index of the training data element -> [1]
			double[][] distances = new double[trainingValues.Count][];

			double accuracy = 0;
			double correct = 0, testNumber = 0;

			for (int i = 0; i < trainingValues.Count; i++)
				distances[i] = new double[2];

			Console.WriteLine("[i] classifying...");

			// start computing
			for (var test = 0; test < testValues.Count; test++)
			{
				Parallel.For(0, trainingValues.Count, index =>
				{
					var dist = EuclideanDistance(testValues[test], trainingValues[index]);
					distances[index][0] = dist;
					distances[index][1] = index;
				}
				);

				Console.WriteLine("[+] closest K={0} neighbors: ", K);

				// sort and select first K of them
				var sortedDistances = distances.AsParallel().OrderBy(t => t[0]).Take(K);

				string realClass = testClass[test];

				// print and check the result
				foreach (var d in sortedDistances)
				{
					string predictedClass = trainingClass[(int)d[1]];
					if (string.Equals(realClass, predictedClass) == true)
						correct++;
					testNumber++;
					Console.WriteLine("[>>>] test {0}: real class: {1} predicted class: {2}", test, realClass, predictedClass);
				}
			}

			Console.WriteLine();

			// compute and print the accuracy
			accuracy = (correct / testNumber) * 100;
			Console.WriteLine("[i] accuracy: {0}%", accuracy);

		}

		private static double EuclideanDistance(double[] sampleOne, double[] sampleTwo)
		{
			double d = 0.0;

			for (int i = 0; i < sampleOne.Length; i++)
			{
				double temp = sampleOne[i] - sampleTwo[i];
				d += temp * temp;
			}
			return Math.Sqrt(d);
		}

	}
}
