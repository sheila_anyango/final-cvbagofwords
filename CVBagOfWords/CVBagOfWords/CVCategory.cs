﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;

namespace CVBagOfWords
{
    public class CVCategory
    {
        public static string train_data = "C:/Users/ben/Documents/CVsAccepted.csv";
        //public static string test_data = "C:/Users/ben/Documents/topicwordscvtest.csv";
        //public static string test_cv = "C:/Users/ben/Documents/attachments/train/CVTrain1.docx";
        static MLContext mlContext;
        static IDataView traindata;
        private static ITransformer _trainedModel;
        private static PredictionEngine<CVJob, JobType> jobpredEngine;

        public static void CategorizeCV()
        {
            //initialize the mlcontext
            mlContext = new MLContext(seed: 0);

            //load the csv file into a DataView
            traindata = mlContext.Data.LoadFromTextFile<CVJob>(train_data, separatorChar: ',', hasHeader: true);

            //initialize the training pipeline
            var pipeline = DataPipeline();

            //Build the model
            var trainingPipeline = BuildModel(traindata, pipeline);

        }

        public static IEstimator<ITransformer> DataPipeline()
        {
            var textpipeline = mlContext.Transforms.Text.FeaturizeText(inputColumnName: "CVDescription", outputColumnName: "Featurized")
                .Append(mlContext.Transforms.Conversion.MapValueToKey(inputColumnName: "JobType", outputColumnName: "Label"))
                .AppendCacheCheckpoint(mlContext);
            return textpipeline;
        }

        public static IEstimator<ITransformer> BuildModel(IDataView trainingDataView, IEstimator<ITransformer> pipeline)
        {
            var textPipeline = pipeline.Append(mlContext.MulticlassClassification.Trainers.NaiveBayes("Label", "Featurized"))
             .Append(mlContext.Transforms.Conversion.MapKeyToValue("Label"));

            _trainedModel = textPipeline.Fit(trainingDataView);

            jobpredEngine = mlContext.Model.CreatePredictionEngine<CVJob, JobType>(_trainedModel);

            CVJob jobexample = new CVJob()
            {
                CVDescription = "managerprojectprocessconsultantsystemtechnicalcustomerbusinessoperationworkmanagementcontrol" +
                "groupalsodevelopexperiencesolutioncentercontractdatumrouteresponsibleserviceexcellentowneranalyzeoutsourceassignmentlargequality"

            };

            var prediction = jobpredEngine.Predict(jobexample);

            Console.WriteLine($"Single Prediction just-trained-model - Result: {prediction.Jobtype}");

            return textPipeline;
        }
    }

    public class CVJob
    {
        [LoadColumn(0)]
        public string CVDescription { get; set; }

        [LoadColumn(1)]
        public string JobType { get; set; }
    }

    public class JobType
    {
        [ColumnName("Label")]
        public string Jobtype;

    }
}
