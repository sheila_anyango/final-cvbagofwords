﻿using System;
using System.Collections.Generic;
using System.Text;
using Accord.MachineLearning;
using Extreme.DataAnalysis;
using Extreme.Statistics.Distributions;
using Extreme.Statistics.Multivariate;
using Extreme.Data;
using System.Linq;
using Extreme.Mathematics;
using Extreme.Statistics;

namespace CVBagOfWords
{
    public class KmeansCluster
    {
        private static string csv_path = "C:/Users/ben/Documents/attachments/BOG_CV.csv";
        [STAThread]
        public static void KCluster()
        {          
            var frame = DataFrame.ReadCsv(csv_path, hasHeaders: true);
            //frame = frame.RemoveColumn("CVS");
            KMeansClusterAnalysis kmeans = new KMeansClusterAnalysis(frame, 3);
            kmeans.Standardize = true;
            //compute the model
            kmeans.Fit();

            //get the predictions 
            var predictions = kmeans.Predictions;

            //get the distances of each observation to the center
            var distance = kmeans.GetDistancesToCenters();

            for (int i = 21; i < predictions.Length; i++)
                Console.WriteLine("Observation {0} belongs to cluster {1}, distance: {2:F4}.",
                    i, predictions[i], distance[i]);

            // You can use this to compute several statistics:
            var descriptives = distance.SplitBy(predictions)
                .Map(x => new Descriptives<double>(x));

            // Individual clusters are accessed through an index, or through enumeration.            
            for (int i = 0; i < descriptives.Length; i++)
            {
                Console.WriteLine("Cluster {0} has {1} members. Sum of squares: {2:F4}",
                    i, descriptives[i].Count, descriptives[i].SumOfSquares);
                Console.WriteLine("Center: {0:F4}", kmeans.Clusters[i].Center);
            }

            // The distances between clusters are also available:
            Console.WriteLine(kmeans.GetClusterDistances().ToString("F4"));

            // You can get a filter for the observations in a single cluster.
            // This uses the GetIndexes method of categorical vectors.
            var level1Indexes = kmeans.Predictions.GetIndexes(1).ToArray();
            Console.WriteLine("Number of items in cluster 1: {0}", level1Indexes.Length);

            Console.Write("Press any key to exit.");
            Console.ReadLine();
        }

    }


        
}
