﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using static Microsoft.ML.DataOperationsCatalog;
using Microsoft.ML.Trainers;
using Microsoft.ML.Transforms.Text;
namespace CVBagOfWords
{
    public class CVClassifier
    {
        //cvs with full text and acceptance scores
        public static string train_data = "C:/Users/ben/Documents/cvwordfulltrain.csv";
        public static string test_data = "C:/Users/ben/Documents/cvwordsfulltest.csv";

        //cvs with top words found in them and the acceptance scores
        public static string train_data2 = "C:/Users/ben/Documents/topicwordscvtrain.csv";
        public static string test_data2 = "C:/Users/ben/Documents/topicwordscvtest.csv";

        public static string test_cv = "C:/Users/ben/Documents/attachments/CV1.docx";
        static MLContext mlContext;

        public static void JobClassifier()
        {
            mlContext = new MLContext(seed: 0);
            
            IDataView jobdata = mlContext.Data.LoadFromTextFile<CVExtract>(train_data2, separatorChar: ',', hasHeader: false);
            IDataView jobtestdata = mlContext.Data.LoadFromTextFile<CVExtract>(test_data2, separatorChar: ',', hasHeader: false);

            var estimator = mlContext.Transforms.Text.FeaturizeText(outputColumnName: "Features", inputColumnName: nameof(CVExtract.cvtext))
                .Append(mlContext.BinaryClassification.Trainers.SdcaLogisticRegression(labelColumnName: "Label", featureColumnName: "Features"));
            //var wordemb_pipeline = TextPipeline();
            Console.WriteLine("-----Begin training the model-----");
            var model = estimator.Fit(jobdata);
            Console.WriteLine("--------End of training model-------");
            Console.WriteLine();

            //evaluate the model   
            Console.WriteLine("----Evaluate the accuracy of the model-----");
            var predictions = model.Transform(jobtestdata);
            CalibratedBinaryClassificationMetrics metrics = mlContext.BinaryClassification.Evaluate(predictions, "Label");

            Console.WriteLine();
            Console.WriteLine("Evaluation of the model using metrics");
            Console.WriteLine("--------------------------------");
            Console.WriteLine($"Accuracy: {metrics.Accuracy:P2}");
            Console.WriteLine($"Auc: {metrics.AreaUnderRocCurve:P2}");
            Console.WriteLine($"F1Score: {metrics.F1Score:P2}");
            Console.WriteLine($"LogLoss:{metrics.LogLoss:P2}");
            Console.WriteLine($"LogLossReduction:{metrics.LogLossReduction:P2}");
            Console.WriteLine($"Positive Precision:{metrics.PositivePrecision:P2}");
            Console.WriteLine($"Positive Recall:{metrics.PositiveRecall:P2}");
            Console.WriteLine($"Negative Precision:{metrics.NegativePrecision:P2}");
            Console.WriteLine($"Negative Recall:{metrics.NegativeRecall:P2}");
            Console.WriteLine("--------------------------------");

            Console.WriteLine();
            PredictJob(test_cv, model);
        }

        public static void PredictJob(string file, ITransformer model)
        {
            PredictionEngine<CVExtract, JobPrediction> predictionJob = 
                mlContext.Model.CreatePredictionEngine<CVExtract, JobPrediction>(model);
            CVExtract cv = new CVExtract
            {
                cvtext = SimilarityRanking.GetTextDoc(file)
            };

            var result = predictionJob.Predict(cv);

            Console.WriteLine();
            Console.WriteLine("Predicct a new resume if it suits the job");
            Console.WriteLine();
            Console.WriteLine($" Prediction: {(Convert.ToBoolean(result.jobprediction) ? "Is right ffor job" : "Is not right for job")} " +
                $"| Probability: {result.jobprobability} ");

            Console.WriteLine("=============== End of Predictions ===============");
            Console.WriteLine();
        }
        
        
        //data pipeline using Glove
        public static IEstimator<ITransformer> TextPipeline()
        {
            //create the transformation pipeline
            var textPipeline = mlContext.Transforms.Text.NormalizeText("cvtext")
                .Append(mlContext.Transforms.Text.TokenizeIntoWords("Tokens", "cvtext"))
                .Append(mlContext.Transforms.Text.RemoveDefaultStopWords("Tokens", "Tokens",
                    Microsoft.ML.Transforms.Text.StopWordsRemovingEstimator.Language.English))
                .Append(mlContext.Transforms.Text.ApplyWordEmbedding("Features", "Tokens",
                WordEmbeddingEstimator.PretrainedModelKind.GloVe50D))
                .Append(mlContext.BinaryClassification.Trainers.SdcaLogisticRegression(labelColumnName: "Label", featureColumnName: "Features"));
            return textPipeline;
        }
    }


    public class CVExtract
    {
        [LoadColumn(0)]
        public string cvtext { get; set; }

        [LoadColumn(1), ColumnName("Label")]
        public bool joboffer { get; set; }
    }
    public class JobPrediction
    {
        [ColumnName("PredictedJob")]
        public bool jobprediction { get; set; }
       
        public float jobprobability { get; set; }

        public float score { get; set; }
    }
}
